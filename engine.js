var site = {
  title: 'Паттерны программирования',
  description: 'Справочник паттернов программирования',
  url: 'http://паттерны.рф/',
  pages: {
    about: {
      url: 'pages/about.html',
      title: 'О сайте'
    }
  },

  content: {
    Visitor: {
      url: 'articles/Behavioral/Visitor.html',
      title: 'Посетитель (Visitor)',
      description: ''
    },
    Mediator: {
      url: 'articles/Behavioral/Mediator.html',
      title: 'Посредник (Mediator)',
      description: ''
    },
    TemplateMethod: {
      url: 'articles/Behavioral/TemplateMethod.html',
      title: 'Шаблонный метод (Template Method)',
      description: ''
    },
    Iterator: {
      url: 'articles/Behavioral/Iterator.html',
      title: 'Итератор (Iterator)',
      description: ''
    },
    Command: {
      url: 'articles/Behavioral/Command.html',
      title: 'Команда (Command)',
      description: ''
    },
    Memento: {
      url: 'articles/Behavioral/Memento.html',
      title: 'Хранитель (Memento)',
      description: ''
    },
    Interpreter: {
      url: 'articles/Behavioral/Interpreter.html',
      title: 'Интерпретатор (Interpreter)',
      description: ''
    },
    Observer: {
      url: 'articles/Behavioral/Observer.html',
      title: 'Наблюдатель (Observer, Publish/Subcribe)',
      description: ''
    },
    ChainOfResponsibility: {
      url: 'articles/Behavioral/ChainOfResponsibility.html',
      title: 'Цепочка обазанностей (Chain of Responsibility)',
      description: ''
    },
    State: {
      url: 'articles/Behavioral/State.html',
      title: 'Состояние (State)',
      description: ''
    },
    Strategy: {
      url: 'articles/Behavioral/Strategy.html',
      title: 'Стратегия (Strategy)',
      description: ''
    },
    Specification: {
      url: 'articles/Behavioral/Specification.html',
      title: 'Спецификация (Specification)',
      description: ''
    },


    Singleton: {
      url: 'articles/Creational/Singleton.html',
      title: 'Одиночка (Singleton)',
      description: ''
    },
    Prototype: {
      url: 'articles/Creational/Prototype.html',
      title: 'Прототип (Prototype)',
      description: ''
    },
    Builder: {
      url: 'articles/Creational/Builder.html',
      title: 'Строитель (Builder)',
      description: ''
    },
    FactoryMethod: {
      url: 'articles/Creational/FactoryMethod.html',
      title: 'Фабричный метод (Factory Method)',
      description: ''
    },
    AbstractFactory: {
      url: 'articles/Creational/AbstractFactory.html',
      title: 'Абстрактная фабрика (Abstract Factory)',
      description: ''
    },


    Decorator: {
      url: 'articles/Structural/Decorator.html',
      title: 'Декоратор (Decorator)',
      description: ''
    },
    Composite: {
      url: 'articles/Structural/Composite.html',
      title: 'Компоновщик (Composite)',
      description: ''
    },
    Adapter: {
      url: 'articles/Structural/Adapter.html',
      title: 'Адаптер (Adapter, Wrapper, Translator)',
      description: ''
    },
    Bridge: {
      url: 'articles/Structural/Bridge.html',
      title: 'Мост (Bridge)',
      description: ''
    },
    Facade: {
      url: 'articles/Structural/Facade.html',
      title: 'Фасад (Facade)',
      description: ''
    },
    Flyweight: {
      url: 'articles/Structural/Flyweight.html',
      title: 'Приспособленец (Flyweight)',
      description: ''
    },
    Proxy: {
      url: 'articles/Structural/Proxy.html',
      title: 'Заместитель (Proxy)',
      description: ''
    }
  }
};

if (typeof window === 'undefined') {
  // run in nodejs - update rss and sitemap
  updateSite();
} else {
  // run in browser
  window.onload = function() {

    function addLinks(ulElementId, listObject) {
      var ul = document.getElementById(ulElementId);
      if (ul != null) {
        for (var itemName in listObject) {
          if (listObject.hasOwnProperty(itemName)) {
            var item = listObject[itemName];
            var li = document.createElement("li");
            var a = document.createElement("a");
            var text = document.createTextNode(item.title);
            li.appendChild(a);
            a.appendChild(text);
            a.href = "/" + item.url;
            ul.insertBefore(li, ul.firstChild);
          }
        }
      }
    }

    addLinks("pages", site.pages);
    //addLinks("content", site.content);
  };
}


function updateSite() {

  var fs = require("fs");

  // update rss
  var rss = `<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel>` +
    `<title>${site.title}</title>` +
    `<description>${site.description}</description>` +
    `<link>${site.url}</link><language>ru-ru</language>`;
  for (var name in site.content) {
    if (site.content.hasOwnProperty(name)) {
      var item = site.content[name];
      rss += `<item><title>${item.title}</title>` +
        `<description>${item.description}</description>` +
        `<link>${site.url}${item.url}</link></item>`;
    }
  }
  rss += `</channel></rss>`;
  fs.writeFile("rss.xml", rss, function(err) {
    if (err) {
      console.log("Error update RSS");
      return console.log(err);
    }
    console.log("RSS article updated!");
  });

  rss = `<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel>` +
    `<title>${site.title} - Страницы</title>` +
    `<description>${site.description}</description>` +
    `<link>${site.url}</link><language>ru-ru</language>`;
  for (var name in site.pages) {
    if (site.pages.hasOwnProperty(name)) {
      var item = site.pages[name];
      rss += `<item><title>${item.title}</title>` +
        `<link>${site.url}${item.url}</link></item>`;
    }
  }
  rss += `</channel></rss>`;
  fs.writeFile("pages.xml", rss, function(err) {
    if (err) {
      console.log("Error update RSS");
      return console.log(err);
    }
    console.log("RSS pages updated!");
  });

  // updateSitemap
  var sitemap = `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`;
  for (var name in site.content) {
    if (site.content.hasOwnProperty(name)) {
      var item = site.content[name];
      sitemap += `<url><loc>${site.url}${item.url}</loc></url>`;
    }
  }
  for (var name in site.pages) {
    if (site.pages.hasOwnProperty(name)) {
      var item = site.pages[name];
      sitemap += `<url><loc>${site.url}${item.url}</loc></url>`;
    }
  }
  sitemap += `</urlset>`;
  fs.writeFile("sitemap.xml", sitemap, function(err) {
    if (err) {
      console.log("Error update Sitemap");
      return console.log(err);
    }
    console.log("Sitemap updated!");
  });

};
